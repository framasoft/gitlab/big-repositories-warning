requires 'inc::Module::Install::DSL';
requires 'Mojolicious';
requires 'Number::Bytes::Human';
requires 'GitLab::API::v4';
requires "IO::Socket::SSL";
requires "Email::Valid";
